This is a Platform jumping game. The Box is to be moved left or right so as to avoid it touching the top and avoid falling down. 

Controls :
    **F** - move left
    **J** - move right

To exit the game press **E**

For any feedback please feel free to contact
